import { YMaps, Map } from "react-yandex-maps";
import "./App.css";

function App() {
  return (
    <div className="App">
      <YMaps>
        <div>
          My awesome application with maps!
          <Map defaultState={{ center: [55.75, 37.57], zoom: 9 }} />
        </div>
      </YMaps>
    </div>
  );
}

export default App;
